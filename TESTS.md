## Tests

    Se realizo test a todas las rutas de la api

## Autor

- testRutaObtenerAutor: se carga un autor a la base de datos y se verifica el funcionamiento de la ruta GET /autor/id

- testRutaObtenerAutorNoExistente: se prueba ruta GET /autor/id con autor no existente, debera devolver que no se encontro el recurso

- testRutaListarAutores: se carga dos autores a la base de datos y se verifica el funcionamiento de la ruta GET /autores

## Editorial

- testRutaObtenerEditorial: se carga un editorial a la base de datos y se verifica el funcionamiento de la ruta GET /editorial/id

- testRutaObtenerEditorialNoExistente: se prueba ruta GET /editorial/id con editorial no existente, debera devolver que no se encontro el recurso

- testRutaListarEditoriales: se carga dos editoriales a la base de datos y se verifica el funcionamiento de la ruta GET /editoriales

## Genero

- testRutaObtenerGenero: se carga un genero a la base de datos y se verifica el funcionamiento de la ruta GET /genero/id

- testRutaObtenerGeneroNoExistente: se prueba ruta GET /genero/id con genero no existente, debera devolver que no se encontro el recurso

- testRutaListarGeneros: se carga dos generos a la base de datos y se verifica el funcionamiento de la ruta GET /generos

## Libro

 Para todos los test de libros se cargan 2 generos, 2 editoriales y 2 autores en base de datos 

- testRutaCrearLibro: se crea un libro mediante la ruta POST /libros y se verifica el mensaje que devuelve la api y que se haya cargado en base de datos

- testRutaActualizarLibro: se carga un libro,se lo modifica mediante la ruta PUT /libro/id y se verifica el mensaje que devuelve la api y que se haya modificado en base de datos

- testRutaActualizarLibroNoExistente: Se intenta modificar un libro que no existe mediante la ruta PUT /libro/id

- testRutaObtenerLibro: se carga un libro a la base de datos y se verifica el funcionamiento de la ruta GET /libro/id

- testRutaObtenerLibroNoExistente: se prueba ruta GET /libro/id con libro no existente, debera devolver que no se encontro el recurso

- testRutaListarLibros: se carga dos libros a la base de datos y se verifica el funcionamiento de la ruta GET /libros

- testRutaEliminarLibro: se carga un libro a la base de datos y se lo elimina con la ruta DELETE /libro/id , se verifica respuesta de la api y que se haya eliminado el libro de la base

## Socios

- testRutaCrearSocio: se crea un socio mediante la ruta POST /socios y se verifica el mensaje que devuelve la api y que se haya cargado en base de datos

- testRutaActualizarSocio: se carga un socio,se lo modifica mediante la ruta PUT /socio/id y se verifica el mensaje que devuelve la api y que se haya modificado en base de datos

- testRutaActualizarSocioNoExistente: Se intenta modificar un socio que no existe mediante la ruta PUT /socio/id

- testRutaObtenerSocio: se carga un socio a la base de datos y se verifica el funcionamiento de la ruta GET /socio/id

- testRutaObtenerSocioNoExistente: se prueba ruta GET /socio/id con socio no existente, debera devolver que no se encontro el recurso

- testRutaListarSocios: se carga dos socios a la base de datos y se verifica el funcionamiento de la ruta GET /socios

- testRutaEliminarSocio: se carga un socio a la base de datos y se lo elimina con la ruta DELETE /socio/id , se verifica respuesta de la api y que se haya eliminado el socio de la base