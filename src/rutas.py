from flask import request, Blueprint, jsonify
from repositorio import *
from excepciones import RegistroNoEncontradoException
import cargarDatosIniciales

ruta = Blueprint("rutas", __name__, url_prefix="")


@ruta.route("/cargarDatos", methods=["POST"])
def cargarDatos():
    return cargarDatosIniciales.cargarDatos()


@ruta.route("/autores")
def listarAutores():
    return jsonify(AutorRepositorio().listar())


@ruta.route("/autor/<int:id>")
def obtenerAutor(id):
    try:
        return AutorRepositorio().obtenerPorId(id)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404


@ruta.route("/generos")
def listarGeneros():
    return jsonify(GeneroRepositorio().listar())


@ruta.route("/genero/<id>")
def obtenerGenero(id):
    try:
        return GeneroRepositorio().obtenerPorId(id)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404


@ruta.route("/editoriales")
def listarEditoriales():
    return jsonify(EditorialRepositorio().listar())


@ruta.route("/editorial/<id>")
def obtenerEditorial(id):
    try:
        return EditorialRepositorio().obtenerPorId(id)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404


@ruta.route("/libros", methods=["POST"])
def crearLibro():
    libro = request.get_json()
    return jsonify(LibroRepositorio().crear(libro)), 201


@ruta.route("/libro/<id>", methods=["PUT"])
def actualizarLibro(id):
    libro = request.get_json()
    try:
        return LibroRepositorio().actualizar(id, libro)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404


@ruta.route("/libro/<id>")
def obtenerLibro(id):
    try:
        return LibroRepositorio().obtenerPorId(id)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404


@ruta.route("/libros")
def listarLibro():
    return jsonify(LibroRepositorio().listar())


@ruta.route("/libro/<id>", methods=["DELETE"])
def eliminarLibro(id):
    try:
        return LibroRepositorio().eliminar(id)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404


@ruta.route("/socios", methods=["POST"])
def crearSocio():
    socio = request.get_json()
    return jsonify(SocioRepositorio().crear(socio)), 201


@ruta.route("/socio/<id>", methods=["PUT"])
def actualizarSocio(id):
    socio = request.get_json()
    try:
        return SocioRepositorio().actualizar(id, socio)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404


@ruta.route("/socio/<id>")
def obtenerSocio(id):
    try:
        return SocioRepositorio().obtenerPorId(id)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404


@ruta.route("/socios")
def listarSocio():
    return jsonify(SocioRepositorio().listar())


@ruta.route("/socio/<id>", methods=["DELETE"])
def eliminarSocio(id):
    try:
        return SocioRepositorio().eliminar(id)
    except RegistroNoEncontradoException as e:
        return jsonify({"msg": str(e)}), 404
 