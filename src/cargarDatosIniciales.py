from entidades import *
from app import db

def hayDatosCargados():
    autor = db.session.query(AutorEntidad).first()
    genero = db.session.query(GeneroEntidad).first()
    editorial = db.session.query(EditorialEntidad).first()
    return autor != None or genero != None or editorial != None 

def cargarDatos():
    if not hayDatosCargados():
        cargarAutores()
        cargarGeneros()
        cargarEditoriales()
        db.session.commit()
        return {"msg": "Autores, generos y editoriales cargadas"}
    else:
        return {"msg": "Ya hay datos cargados"}

def cargarAutores():
    autores = [{
        'nombre': "Jorge Luis",
        'apellido': "Borges",
        'nacionalidad': "Argentino" 
    },{
        'nombre': "Julio Florencio",
        'apellido': "Cortázar",
        'nacionalidad': "Argentino" 
    },{
        'nombre': "Pablo",
        'apellido': "Neruda",
        'nacionalidad': "Chileno" 
    }]
    for autorJson in autores:
        autor = AutorEntidad(autorJson)
        db.session.add(autor)

def cargarGeneros():
    generos = [{
        'nombre': "Poesia" 
    },{
        'nombre': "Cuento"
    },{
        'nombre': "Novela"
    }]
    for generoJson in generos:
        genero = GeneroEntidad(generoJson)
        db.session.add(genero)

def cargarEditoriales():
    editoriales = [{
        'nombre': "Minotauro" 
    },{
        'nombre': "Planeta "
    }]
    for editorialJson in editoriales:
        editorial = EditorialEntidad(editorialJson)
        db.session.add(editorial)