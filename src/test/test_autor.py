from . import BaseTestClass
from entidades import AutorEntidad
from app import db
import json

class TestAutores(BaseTestClass):

    def testRutaObtenerAutor(self):
        with self.app.app_context():
            #Cargo datos en base
            autorJsonCrear = {"nombre": "Jorge","apellido": "Borges","nacionalidad": "Argentino"}
            autor = AutorEntidad(autorJsonCrear)
            db.session.add(autor)
            db.session.commit()
            #Ruta a testear
            res = self.client.get('/autor/1')
            #Comparativas
            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("nombre") == autorJsonCrear.get("nombre")
            assert data.get("apellido") == autorJsonCrear.get("apellido")
            assert data.get("nacionalidad") == autorJsonCrear.get("nacionalidad")
            assert data.get("id") == 1

    def testRutaObtenerAutorNoExistente(self):
        with self.app.app_context():
            autorJsonCrear = {"nombre": "Jorge","apellido": "Borges","nacionalidad": "Argentino"}
            autor = AutorEntidad(autorJsonCrear)
            db.session.add(autor)
            db.session.commit()

            res = self.client.get('/autor/3')

            assert res.status_code == 404
            data = json.loads(res.data.decode())
            assert data.get("msg") == "El autor con el id: 3, no existe"

    def testRutaListarAutores(self):
        with self.app.app_context():
            autorJsonCrear = {"nombre": "Jorge","apellido": "Borges","nacionalidad": "Argentino"}
            autor = AutorEntidad(autorJsonCrear)
            autor2JsonCrear = {"nombre": "Julio","apellido": "Cortazar","nacionalidad": "Argentino"}
            autor2 = AutorEntidad(autor2JsonCrear)
            db.session.add(autor)
            db.session.add(autor2)
            db.session.commit()

            res = self.client.get('/autores')
            
            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert len(data) == 2
            assert data[0].get("nombre") == autorJsonCrear.get("nombre")
            assert data[0].get("apellido") == autorJsonCrear.get("apellido")
            assert data[0].get("nacionalidad") == autorJsonCrear.get("nacionalidad")
            assert data[0].get("id") == 1
            assert data[1].get("nombre") == autor2JsonCrear.get("nombre")
            assert data[1].get("apellido") == autor2JsonCrear.get("apellido")
            assert data[1].get("nacionalidad") == autor2JsonCrear.get("nacionalidad")
            assert data[1].get("id") == 2