from . import BaseTestClass
from entidades import EditorialEntidad
from app import db
import json

class TestEditoriales(BaseTestClass):

    def testRutaObtenerEditorial(self):
        with self.app.app_context():
            editorialJsonCrear = {"nombre": "Planeta"}
            editorial = EditorialEntidad(editorialJsonCrear)
            db.session.add(editorial)
            db.session.commit()

            res = self.client.get('/editorial/1')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("nombre") == editorialJsonCrear.get("nombre")
            assert data.get("id") == 1

    def testRutaObtenerEditorialNoExistente(self):
        with self.app.app_context():
            editorialJsonCrear = {"nombre": "Planeta"}
            editorial = EditorialEntidad(editorialJsonCrear)
            db.session.add(editorial)
            db.session.commit()

            res = self.client.get('/editorial/3')

            assert res.status_code == 404
            data = json.loads(res.data.decode())
            assert data.get("msg") == "La editorial con el id: 3, no existe"

    def testRutaListarEditoriales(self):
        with self.app.app_context():
            editorialJsonCrear = {"nombre": "Planeta"}
            editorial = EditorialEntidad(editorialJsonCrear)
            editorial2JsonCrear = {"nombre": "Santillana"}
            editorial2 = EditorialEntidad(editorial2JsonCrear)
            db.session.add(editorial)
            db.session.add(editorial2)
            db.session.commit()

            res = self.client.get('/editoriales')
            
            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert len(data) == 2
            assert data[0].get("nombre") == editorialJsonCrear.get("nombre")
            assert data[0].get("id") == 1
            assert data[1].get("nombre") == editorial2JsonCrear.get("nombre")
            assert data[1].get("id") == 2