from . import BaseTestClass
from entidades import SocioEntidad
from app import db
import json

class TestSocios(BaseTestClass):

    def testRutaCrearSocio(self):
        with self.app.app_context():
            socio = {
                "nombre": "nombre",
                "apellido": "apellido",
                "dni": 40555888,
                "telefono": 1144774477,
            }     
            res = self.client.post('/socios',data=json.dumps(socio),content_type='application/json')

            assert res.status_code == 201
            data = json.loads(res.data.decode())
            assert data.get("msg") == f'Se creo el socio {socio.get("nombre")}'
            #Verifico que se haya creado correctamente
            socioCargado = SocioEntidad.query.get(1)
            assert socioCargado.nombre == socio.get("nombre")
            assert socioCargado.apellido == socio.get("apellido")
            assert socioCargado.dni == socio.get("dni")
            assert socioCargado.telefono == socio.get("telefono")

    def testRutaActualizarSocio(self):
        with self.app.app_context():
            socio = {
                "nombre": "nombre",
                "apellido": "apellido",
                "dni": 40555888,
                "telefono": 1144774477,
            }     
            socio = SocioEntidad(socio)
            db.session.add(socio)
            db.session.commit()

            socioActualizar = {
                "nombre": "nombre2",
                "apellido": "apellido2",
                "dni": 40444888,
                "telefono": 1144664477,
            }
            res = self.client.put('/socio/1',data=json.dumps(socioActualizar),content_type='application/json')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("msg") == f'Se modifico el socio {socioActualizar.get("nombre")}'
            #Verifico que se haya creado correctamente
            socioCargado = SocioEntidad.query.get(1)
            assert socioCargado.nombre == socioActualizar.get("nombre")
            assert socioCargado.apellido == socioActualizar.get("apellido")
            assert socioCargado.dni == socioActualizar.get("dni")
            assert socioCargado.telefono == socioActualizar.get("telefono")

    def testRutaActualizarSocioNoExistente(self):
        with self.app.app_context():
            socioActualizar = {                
                "nombre": "nombre2",
                "apellido": "apellido2",
                "dni": 40444888,
                "telefono": 1144664477,
            }
            res = self.client.put('/socio/1',data=json.dumps(socioActualizar),content_type='application/json')

            assert res.status_code == 404
            data = json.loads(res.data.decode())
            assert data.get("msg") == "El socio con el id: 1, no existe"

    def testRutaObtenerSocio(self):
        with self.app.app_context():
            socioJson = {
                "nombre": "nombre",
                "apellido": "apellido",
                "dni": 40555888,
                "telefono": 1144774477,
            }     
            socio = SocioEntidad(socioJson)
            db.session.add(socio)
            db.session.commit()

            res = self.client.get('/socio/1')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("nombre") == socioJson.get("nombre")
            assert data.get("autorId") == socioJson.get("autorId")
            assert data.get("generoId") == socioJson.get("generoId")
            assert data.get("editorialId") == socioJson.get("editorialId")
            assert data.get("id") == 1

    def testRutaObtenerSocioNoExistente(self):
        with self.app.app_context():

            res = self.client.get('/socio/3')

            assert res.status_code == 404
            data = json.loads(res.data.decode())
            assert data.get("msg") == "El socio con el id: 3, no existe"

    def testRutaListarSocios(self):
        with self.app.app_context():
            
            socioJson = {
                "nombre": "nombre",
                "apellido": "apellido",
                "dni": 40555888,
                "telefono": 1144774477,
            }     
            socio = SocioEntidad(socioJson)
            db.session.add(socio)
            
            socio2Json = {
                "nombre": "nombre2",
                "apellido": "apellido2",
                "dni": 40555222,
                "telefono": 1144772277,
            }     
            socio2 = SocioEntidad(socio2Json)
            db.session.add(socio2)
            db.session.commit()

            res = self.client.get('/socios')
            
            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert len(data) == 2
            assert data[0].get("nombre") == socioJson.get("nombre")
            assert data[0].get("id") == 1
            assert data[1].get("nombre") == socio2Json.get("nombre")
            assert data[1].get("id") == 2

    def testRutaEliminarSocio(self):
        with self.app.app_context():
            socioJson = {
                "nombre": "nombre",
                "apellido": "apellido",
                "dni": 40555888,
                "telefono": 1144774477,
            }     
            socio = SocioEntidad(socioJson)
            db.session.add(socio)
            db.session.commit()

            res = self.client.delete('/socio/1')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("msg") == f'Se elimino el socio {socioJson.get("nombre")}'
            #Verifico que se haya creado correctamente
            socioCargado = SocioEntidad.query.get(1)
            assert socioCargado == None