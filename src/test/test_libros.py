from . import BaseTestClass
from entidades import GeneroEntidad, EditorialEntidad, LibroEntidad, AutorEntidad
from app import db
import json

class TestLibros(BaseTestClass):

    def setUp(self):
        #Se cargan datos necesarios para todos los test de esta clase
        super().setUp() 
        with self.app.app_context():
            generoJsonCrear = {"nombre": "Narrativo"}
            genero = GeneroEntidad(generoJsonCrear)
            db.session.add(genero)
            generoJsonCrear = {"nombre": "Terror"}
            genero = GeneroEntidad(generoJsonCrear)
            db.session.add(genero)

            editorialJsonCrear = {"nombre": "Planeta"}
            editorial = EditorialEntidad(editorialJsonCrear)
            db.session.add(editorial)
            editorialJsonCrear = {"nombre": "Santillana"}
            editorial = EditorialEntidad(editorialJsonCrear)
            db.session.add(editorial)

            autorJsonCrear = {"nombre": "Jorge","apellido": "Borges","nacionalidad": "Argentino"}
            autor = AutorEntidad(autorJsonCrear)
            db.session.add(autor)
            autorJsonCrear = {"nombre": "Julio","apellido": "Cortazar","nacionalidad": "Argentino"}
            autor = AutorEntidad(autorJsonCrear)
            db.session.add(autor)

            db.session.commit()

    def testRutaCrearLibro(self):
        with self.app.app_context():
            libro = {
                "nombre": "Ficciones",
                "volumen": 1,
                "autorId": 1,
                "generoId": 1,
                "editorialId": 1,
            }     
            res = self.client.post('/libros',data=json.dumps(libro),content_type='application/json')

            assert res.status_code == 201
            data = json.loads(res.data.decode())
            assert data.get("msg") == f'Se creo el libro {libro.get("nombre")}'
            #Verifico que se haya creado correctamente
            libroCargado = LibroEntidad.query.get(1)
            assert libroCargado.nombre == libro.get("nombre")
            assert libroCargado.volumen == libro.get("volumen")
            assert libroCargado.autorId == libro.get("autorId")
            assert libroCargado.generoId == libro.get("generoId")
            assert libroCargado.editorialId == libro.get("editorialId")
            

    def testRutaActualizarLibro(self):
        with self.app.app_context():
            libro = {
                "nombre": "Ficcione",
                "volumen": 1,
                "autorId": 2,
                "generoId": 2,
                "editorialId": 1,
            }     
            libro = LibroEntidad(libro)
            db.session.add(libro)


            libroActualizar = {
                "nombre": "Ficciones",
                "volumen": 3,
                "autorId": 1,
                "generoId": 1,
                "editorialId": 2,
            }
            res = self.client.put('/libro/1',data=json.dumps(libroActualizar),content_type='application/json')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("msg") == f'Se modifico el libro {libroActualizar.get("nombre")}'
            #Verifico que se haya creado correctamente
            libroCargado = LibroEntidad.query.get(1)
            assert libroCargado.nombre == libroActualizar.get("nombre")
            assert libroCargado.volumen == libroActualizar.get("volumen")
            assert libroCargado.autorId == libroActualizar.get("autorId")
            assert libroCargado.generoId == libroActualizar.get("generoId")
            assert libroCargado.editorialId == libroActualizar.get("editorialId")

    def testRutaActualizarLibroNoExistente(self):
        with self.app.app_context():
            libroActualizar = {
                "nombre": "Ficciones",
                "volumen": 1,
                "autorId": 2,
                "generoId": 1,
                "editorialId": 2,
            }
            res = self.client.put('/libro/1',data=json.dumps(libroActualizar),content_type='application/json')

            assert res.status_code == 404
            data = json.loads(res.data.decode())
            assert data.get("msg") == "El libro con el id: 1, no existe"

    def testRutaObtenerLibro(self):
        with self.app.app_context():
            libroJsonCrear = {
                "nombre": "Ficciones",
                "volumen": 1,
                "autorId": 1,
                "generoId": 2,
                "editorialId": 1,
            }     
            libro = LibroEntidad(libroJsonCrear)
            db.session.add(libro)
            db.session.commit()

            res = self.client.get('/libro/1')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("nombre") == libroJsonCrear.get("nombre")
            assert data.get("volumen") == libro.volumen
            assert data.get("autorId") == libroJsonCrear.get("autorId")
            assert data.get("generoId") == libroJsonCrear.get("generoId")
            assert data.get("editorialId") == libroJsonCrear.get("editorialId")
            assert data.get("id") == 1

    def testRutaObtenerLibroNoExistente(self):
        with self.app.app_context():

            res = self.client.get('/libro/3')

            assert res.status_code == 404
            data = json.loads(res.data.decode())
            assert data.get("msg") == "El libro con el id: 3, no existe"

    def testRutaListarLibros(self):
        with self.app.app_context():
            libroJsonCrear = {
                "nombre": "Ficciones",
                "autorId": 1,
                "generoId": 2,
                "volumen": 1,
                "editorialId": 1,
            }     
            libro = LibroEntidad(libroJsonCrear)
            db.session.add(libro)
            libro2JsonCrear = {
                "nombre": "El hacedor",
                "autorId": 1,
                "generoId": 1,
                "volumen": 2,
                "editorialId": 1,
            }     
            libro2 = LibroEntidad(libro2JsonCrear)
            db.session.add(libro2)
            db.session.commit()

            res = self.client.get('/libros')
            
            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert len(data) == 2
            assert data[0].get("nombre") == libroJsonCrear.get("nombre")
            assert data[0].get("id") == 1
            assert data[1].get("nombre") == libro2JsonCrear.get("nombre")
            assert data[1].get("id") == 2

    def testRutaEliminarLibro(self):
        with self.app.app_context():
            libroJson = {
                "nombre": "Ficciones",
                "autorId": 1,
                "generoId": 2,
                "volumen": 1,
                "editorialId": 1,
            }     
            libro = LibroEntidad(libroJson)
            db.session.add(libro)

            res = self.client.delete('/libro/1')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("msg") == f'Se elimino el libro {libroJson.get("nombre")}'
            #Verifico que se haya creado correctamente
            libroCargado = LibroEntidad.query.get(1)
            assert libroCargado == None