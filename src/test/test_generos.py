from . import BaseTestClass
from entidades import GeneroEntidad
from app import db
import json

class TestGeneros(BaseTestClass):

    def testRutaObtenerGenero(self):
        with self.app.app_context():
            generoJsonCrear = {"nombre": "Narrativo"}
            genero = GeneroEntidad(generoJsonCrear)
            db.session.add(genero)
            db.session.commit()

            res = self.client.get('/genero/1')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert data.get("nombre") == generoJsonCrear.get("nombre")
            assert data.get("id") == 1

    def testRutaObtenerGeneroNoExistente(self):
        with self.app.app_context():
            generoJsonCrear = {"nombre": "Narrativo"}
            genero = GeneroEntidad(generoJsonCrear)
            db.session.add(genero)
            db.session.commit()

            res = self.client.get('/genero/3')

            assert res.status_code == 404
            data = json.loads(res.data.decode())
            assert data.get("msg") == "El genero con el id: 3, no existe"

    def testRutaListarGeneros(self):
        with self.app.app_context():
            generoJsonCrear = {"nombre": "Narrativo"}
            genero = GeneroEntidad(generoJsonCrear)
            genero2JsonCrear = {"nombre": "Terror"}
            genero2 = GeneroEntidad(genero2JsonCrear)
            db.session.add(genero)
            db.session.add(genero2)
            db.session.commit()

            res = self.client.get('/generos')

            assert res.status_code == 200
            data = json.loads(res.data.decode())
            assert len(data) == 2
            assert data[0].get("nombre") == generoJsonCrear.get("nombre")
            assert data[0].get("id") == 1
            assert data[1].get("nombre") == genero2JsonCrear.get("nombre")
            assert data[1].get("id") == 2