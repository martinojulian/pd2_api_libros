from app import db
from sqlalchemy.orm import relationship

class AutorEntidad(db.Model):
    __tablename__ = "autor" 
    id = db.Column(db.Integer,primary_key=True)
    nombre = db.Column(db.String(250))
    apellido = db.Column(db.String(250))
    nacionalidad = db.Column(db.String(250))

    def __init__(self, autor):
        self.nombre = autor.get('nombre')
        self.apellido = autor.get('apellido')
        self.nacionalidad = autor.get('nacionalidad')

    def json(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
            'apellido': self.apellido,
            'nacionalidad': self.nacionalidad   
        }

class GeneroEntidad(db.Model):
    __tablename__ = "genero" 
    id = db.Column(db.Integer,primary_key=True)
    nombre = db.Column(db.String(250))

    def __init__(self, genero):
        self.nombre = genero.get('nombre')

    def json(self):
        return {
            'id': self.id,
            'nombre': self.nombre           
        }

class EditorialEntidad(db.Model):
    __tablename__ = "editorial" 
    id = db.Column(db.Integer,primary_key=True)
    nombre = db.Column(db.String(250))
    
    def __init__(self, editorial):
        self.nombre = editorial.get('nombre')
    
    def json(self):
        return {
            'id': self.id,
            'nombre': self.nombre           
        }

class LibroEntidad(db.Model):
    __tablename__ = "libro" 
    id = db.Column(db.Integer,primary_key=True)
    nombre = db.Column(db.String(250))
    volumen = db.Column(db.Integer)
    autorId = db.Column(db.Integer, db.ForeignKey("autor.id"))
    autor = relationship("AutorEntidad")
    generoId = db.Column(db.Integer, db.ForeignKey("genero.id"))
    genero = relationship("GeneroEntidad")
    editorialId = db.Column(db.Integer, db.ForeignKey("editorial.id"))
    editorial = relationship("EditorialEntidad")

    def __init__(self, libro):
        self.nombre = libro.get('nombre')
        self.volumen = libro.get('volumen')
        self.autorId = libro.get('autorId')
        self.generoId = libro.get('generoId')
        self.editorialId = libro.get('editorialId')        
    
    def json(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
            'volumen': self.volumen,
            'autorId': self.autorId,
            'generoId': self.generoId,         
            'editorialId': self.editorialId,    
        }

class SocioEntidad(db.Model):
    __tablename__ = "socio" 
    id = db.Column(db.Integer,primary_key=True)
    nombre = db.Column(db.String(250))
    apellido = db.Column(db.String(250))
    dni = db.Column(db.Integer)
    telefono = db.Column(db.Integer)
    
    def __init__(self, libro):
        self.nombre = libro.get('nombre')
        self.apellido = libro.get('apellido')
        self.dni = libro.get('dni')
        self.telefono = libro.get('telefono') 

    def json(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
            'apellido': self.apellido,
            'dni': self.dni,         
            'telefono': self.telefono         
        }