from app import db
from entidades import *
from excepciones import RegistroNoEncontradoException


class AutorRepositorio:
    def listar(self):
        autores = AutorEntidad.query.all()
        return [autor.json() for autor in autores]

    def obtenerPorId(self, id):
        autor = AutorEntidad.query.get(id)
        if autor:
            return autor.json()
        else:
            raise RegistroNoEncontradoException(f"El autor con el id: {id}, no existe")


class GeneroRepositorio:
    def listar(self):
        generos = GeneroEntidad.query.all()
        return [genero.json() for genero in generos]

    def obtenerPorId(self, id):
        genero = GeneroEntidad.query.get(id)
        if genero:
            return genero.json()
        else:
            raise RegistroNoEncontradoException(f"El genero con el id: {id}, no existe")


class EditorialRepositorio:
    def listar(self):
        editoriales = EditorialEntidad.query.all()
        return [editorial.json() for editorial in editoriales]

    def obtenerPorId(self, id):
        editorial = EditorialEntidad.query.get(id)
        if editorial:
            return editorial.json()
        else:
            raise RegistroNoEncontradoException(
                f"La editorial con el id: {id}, no existe"
            )


class LibroRepositorio:
    def crear(self, libro):
        libro = LibroEntidad(libro)
        db.session.add(libro)
        db.session.commit()
        return {"msg": f"Se creo el libro {libro.nombre}"}

    def actualizar(self, id, json):
        libro = LibroEntidad.query.get(id)
        if libro:
            libro.nombre = json.get("nombre")
            libro.autorId = json.get("autorId")
            libro.volumen = json.get("volumen")
            libro.generoId = json.get("generoId")
            libro.editorialId = json.get("editorialId")
            db.session.add(libro)
            db.session.commit()
        else:
            raise RegistroNoEncontradoException(f"El libro con el id: {id}, no existe")
        return {"msg": f"Se modifico el libro {libro.nombre}"}

    def obtenerPorId(self, id):
        libro = LibroEntidad.query.get(id)
        if libro:
            return libro.json()
        else:
            raise RegistroNoEncontradoException(f"El libro con el id: {id}, no existe")

    def listar(self):
        libros = LibroEntidad.query.all()
        return [libro.json() for libro in libros]

    def eliminar(self, id):
        libro = LibroEntidad.query.get(id)
        if libro:
            db.session.delete(libro)
            db.session.commit()
            return {"msg": f"Se elimino el libro {libro.nombre}"}
        else:
            raise RegistroNoEncontradoException(f"El libro con el id: {id}, no existe")


class SocioRepositorio:
    def crear(self, socio):
        socio = SocioEntidad(socio)
        db.session.add(socio)
        db.session.commit()
        return {"msg": f"Se creo el socio {socio.nombre}"}

    def actualizar(self, id, json):
        socio = SocioEntidad.query.get(id)
        if socio:
            socio.nombre = json.get("nombre")
            socio.apellido = json.get("apellido")
            socio.dni = json.get("dni")
            socio.telefono = json.get("telefono")
            db.session.add(socio)
            db.session.commit()
        else:
            raise RegistroNoEncontradoException(f"El socio con el id: {id}, no existe")
        return {"msg": f"Se modifico el socio {socio.nombre}"}

    def obtenerPorId(self, id):
        socio = SocioEntidad.query.get(id)
        if socio:
            return socio.json()
        else:
            raise RegistroNoEncontradoException(f"El socio con el id: {id}, no existe")

    def listar(self):
        socios = SocioEntidad.query.all()
        return [socio.json() for socio in socios]

    def eliminar(self, id):
        socio = SocioEntidad.query.get(id)
        if socio:
            db.session.delete(socio)
            db.session.commit()
            return {"msg": f"Se elimino el socio {socio.nombre}"}
        else:
            raise RegistroNoEncontradoException(f"El socio con el id: {id}, no existe")
