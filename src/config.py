from os import environ

class Config:
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    FLASK_ENV = environ.get("FLASK_ENV")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ConfigTest(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI_TEST")
    DEBUG = True
