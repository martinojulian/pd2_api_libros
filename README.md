<h1>Api catalogo libros</h1>

<h3> Entidades </h3>

<ul>
    <li>
        Autor
        <ol>
            <li>id</li>
            <li>nombre</li>
            <li>apellido</li>
            <li>nacionalidad</li>
        </ol>
    </li>
    <li>
        Genero
        <ol>
            <li>id</li>
            <li>nombre</li>
        </ol>
    </li>
    <li>
        Editorial
        <ol>
            <li>id</li>
            <li>nombre</li>
        </ol>
    </li>
    <li>
        Libro
        <ol>
            <li>id</li>
            <li>nombre</li>
            <li>volumen</li>
            <li>autorId</li>
            <li>generoId</li>
            <li>editorialId</li>
        </ol>
    </li>
    <li>
        Socio
        <ol>
            <li>id</li>
            <li>nombre</li>
            <li>apellido</li>
            <li>dni</li>
            <li>telefono</li>
        </ol>
    </li>
</ul>

<h3> Rutas </h3>

<ul>
    <li>
        Autor
        <ol>
            <li>GET /autores</li>
            <li>GET /autor/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Genero
        <ol>
            <li>GET /generos</li>
            <li>GET /genero/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Editorial
        <ol>
            <li>GET /editoriales</li>
            <li>GET /editorial/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Libro
        <ol>
            <li>GET /libros</li>
            <li>GET /libro/&lt;int:id&gt;</li>
            <li>POST /libros</li>
            <li>PUT /libro/&lt;int:id&gt;</li>
            <li>DELETE /libro/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Socio
        <ol>
            <li>GET /socios</li>
            <li>GET /socio/&lt;int:id&gt;</li>
            <li>POST /socios</li>
            <li>PUT /lisociobro/&lt;int:id&gt;</li>
            <li>DELETE /socio/&lt;int:id&gt;</li>
        </ol>
    </li>
</ul>
